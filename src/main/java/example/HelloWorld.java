package example;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by FochMaiden on 02/12/15.
 */
public class HelloWorld extends HttpServlet {

    private int hitCount;

    public void init(){
        hitCount = 0;
    }


    @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      HttpSession session = req.getSession(false);
      String firstname = req.getParameter("firstname");

        if (hitCount == 5){
            PrintWriter out = resp.getWriter();
            out.print("User limit exceeded");
        }
        if (hitCount == 0) {
            resp.sendRedirect("bye.jsp");
            session = req.getSession();
            hitCount++;
        }else {
          resp.sendRedirect("nope.jsp");
      }
      session.setAttribute("Ciastko", firstname);
  }

}
